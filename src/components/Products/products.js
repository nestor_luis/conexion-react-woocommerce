import React from 'react'
import axios from 'axios';
import classes from './Products.module.css';
require('dotenv').config();
 
class Products extends React.Component{
     state ={
        storeProducts:[] 
    }
    

    componentDidMount() {
                  
        axios({
            method: 'get',
            url:process.env.REACT_APP_WOO_URL,
            auth: {
                username:process.env.REACT_APP_WOO_PUBLIC,
                password:process.env.REACT_APP_WOO_SECRET

            }
        })
        .then( response => {
            this.setState({
                storeProducts: response.data
            });
           //console.log('nombre de producto: ',response.data);
            
        } );
  }

   render(){

        if(this.state.storeProducts.length>0){
            const products =this.state.storeProducts.map(product => {
            return ( 
                <div class="row row-cols-1 row-cols-md-2 g-4">
                <div class="col-sm-3">
                    <div  key={product.id} className="rounded float-start">
                    <img  src={product.images[0].src}  width="200" />
                    <div class="card-body">
                        <h5 class="card-title">{product.name}</h5>
                        <p class="card-text">{product.price} BsF</p>
                       
                    </div>
                    </div>
                </div>
                </div>           
          )
             })
             return <div  className={classes.productsList} >{products}</div>;
        }
        else
      {return(
          <div>
              Hola
          </div>
      );}
       
       
       }
    
}

export default Products
